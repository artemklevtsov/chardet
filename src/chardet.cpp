#include <Rcpp.h>
#include <string.h>
#include <chardet.h>

using namespace Rcpp;

//' @export
// [[Rcpp::export]]
void test() {
    Detect* d;
    DetectObj* obj;
    std::string str = "안녕하세요";
    if ((d = detect_init()) == NULL ) {
        Rcerr << "Chardet handle initialize failed\n";
    }
    switch(detect_r (str.c_str(), str.size(), &obj)) {
    case CHARDET_OUT_OF_MEMORY:
        Rcerr << "On handle processing, occured out of memory\n";
        detect_obj_free (&obj);
    case CHARDET_NULL_OBJECT:
        Rcerr << "2st argument of chardet() is must memory allocation "
                 "with detect_obj_init API\n";
    }
    std::cout << obj->encoding << "\n";
    //std::string res(obj->encoding);
    detect_obj_free(&obj);
}
